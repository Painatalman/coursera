class Comment < ActiveRecord::Base
	# 
	belongs_to :post 	# dois pontos eh assim! :post eh um Symbol de ruby!!
	
	# validators
	validates_presence_of :post_id
	validates_presence_of :body
end
