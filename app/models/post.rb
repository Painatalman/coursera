class Post < ActiveRecord::Base
	# adicionar aqui as relacoes muitos para um e essas merdas
	# exemplo de comando no rails console
		# p = Post.all
		# p[0].comments
	has_many :comments,
	dependent: :destroy # quando um post for apagado, os comentarios tambem serao apagados!
	validates_presence_of :title
	validates_presence_of :body
end
